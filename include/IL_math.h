/**
 * \file
 *
 * \section DESCRIPTION
 * This header file provides access the math module of the Inertial Labs C/C++
 * Library.
 *
 */
#ifndef _IL_MATH_H_
#define _IL_MATH_H_

#include "IL_linearAlgebra.h"
#include "IL_kinematics.h"

#endif /* _IL_MATH_H_ */