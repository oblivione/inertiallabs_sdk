// ins_windows_basic.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
//#include <windef.h>
#include <windows.h>
#include <tchar.h>
#include "InertialLabs_INS.h"

#define INS_OUTPUT_FORMAT IL_OPVT_RECEIVE
/* Change the connection settings to your configuration. */

//todo: -  the code is not working with double digit com port number . need to see that .

const char* COM_PORT = "COM2";
const int BAUD_RATE = 115200;


int main()
{
	IL_ERROR_CODE errorCode;
	IL_INS ins;
	INSCompositeData sensor_data;
	INSPositionData pos_data;
	int i;

	errorCode = INS_connect(
		&ins,
		COM_PORT,
		BAUD_RATE);

	/* Make sure the user has permission to use the COM port. */
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Could not connect to the sensor on this %s port error:%d\n did you add the user to the dialout group??? \n", COM_PORT, errorCode);

		return 0;
	}

	ins.cmd_flag = IL_STOP_CMD;
	errorCode = INS_Stop(&ins);
	Sleep(3000);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("stop command error\n");

		return 0;
	}

	errorCode = INS_SetMode(&ins, IL_SET_ONREQUEST_MODE);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when setting the mode.\n");

		return 0;
	}

	if (ins.mode)
	{
		printf("On Request mode calibaration running \n");
		Sleep(30000);
	}

	ins.cmd_flag = IL_OPVT_RECEIVE;
	printf("Yaw, Pitch, Roll\n");

	for (i = 0; i < 100; i++)
	{
		errorCode = INS_OPVTdata_Receive(&ins);

		if (errorCode != ILERR_NO_ERROR)
		{
			printf("input data type error command error . \n");

			return 0;
		}

		Sleep(1000);

		INS_YPR(&ins, &sensor_data);

		INS_getGyroAccMag(&ins, &sensor_data);

		INS_getPositionData(&ins, &pos_data);


		printf(" sensor data : \n");
		printf(" Head \t\t Pitch \t\t Roll \t\t  Mag.x\t\t Mag.y\t\t Mag.z\t\t Accel.x\t\t Accel.y\t\t Accel.z\t\t Gyro.x\t\t Gyro.y\t\t Gyro.z\t \n");
		printf("  %f\t\t %f\t\t  %f\t\t %f\t\t  %f\t\t  %f\t\t %f\t\t %f\t\t %f\t\t %f\t\t %f\t\t %f\t \n\n", sensor_data.ypr.yaw, sensor_data.ypr.pitch, sensor_data.ypr.roll,
			sensor_data.magnetic.c0, sensor_data.magnetic.c1, sensor_data.magnetic.c2,
			sensor_data.acceleration.c0, sensor_data.acceleration.c1, sensor_data.acceleration.c2,
			sensor_data.gyro.c0, sensor_data.gyro.c1, sensor_data.gyro.c2);

		/* Wait for 1 second before we query the sensor again. */
		Sleep(1000);

	}

	errorCode = INS_disconnect(&ins);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when trying to disconnect from the sensor.\n");

		return 0;
	}

	return 0;
}
