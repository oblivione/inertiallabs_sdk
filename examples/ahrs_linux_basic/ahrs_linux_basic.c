#include <stdio.h>
#include <unistd.h>
#include "InertialLabs_AHRS.h"

/* Change the connection settings to your configuration. */
const char* const COM_PORT = "/dev/ttyUSB0";
const int BAUD_RATE = 921600;



int main()
{
	IL_ERROR_CODE errorCode;
	IL_AHRS ahrs;
	AHRSCompositeData ahrs_data;;
	int i;

	errorCode = AHRS_connect(
		&ahrs,
		COM_PORT,
		BAUD_RATE);

	/* Make sure the user has permission to use the COM port. */
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Could not connect to the sensor on this %s port error:%d\n did you add the user to the dialout group??? \n", COM_PORT, errorCode);

		return 0;
	}

	ahrs.cmd_flag = IL_AHRS_STOP_CMD;
	errorCode = AHRS_Stop(&ahrs);
	sleep(3);
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("stop command error\n");

		return 0;
	}

	ahrs.cmd_flag = IL_AHRS_REQ_1_CMD;

	for (i = 0; i < 50; i++)
	{
		errorCode = AHRSreq1_Receive(&ahrs);

		if (errorCode != ILERR_NO_ERROR)
		{
			printf("input data type error command error . \n");

			return 0;
		}

		sleep(1);
#if 1
		AHRS_YPR(&ahrs, &ahrs_data);

		AHRS_getGyroAccMag(&ahrs, &ahrs_data);

		AHRS_getSensorData(&ahrs, &ahrs_data);


		printf(" AHRS data : \n");

		printf("  Yaw : %f \n Pitch : %f \n Roll : %f \n \n",
			ahrs_data.ypr.yaw, ahrs_data.ypr.pitch, ahrs_data.ypr.roll);

		printf("  Mag.x : %f \n Mag.y : %f \n Mag.z : %f \n "
			"Accel.x : %f \n Accel.y : %f \n Accel.z : %f\n "
			"Gyro.x : %f \n Gyro.y : %f \n Gyro.z : %f \n "
			"Vinp : %f \n Temper : %f \n \n",
			ahrs_data.magnetic.c0, ahrs_data.magnetic.c1, ahrs_data.magnetic.c2,
			ahrs_data.acceleration.c0, ahrs_data.acceleration.c1, ahrs_data.acceleration.c2,
			ahrs_data.gyro.c0, ahrs_data.gyro.c1, ahrs_data.gyro.c2 , ahrs_data.Vinp ,ahrs_data.Temper );
#endif

		/* Wait for 1 second before we query the sensor again. */
		sleep(1);

	}

	errorCode = AHRS_disconnect(&ahrs);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when trying to disconnect from the sensor.\n");

		return 0;
	}

	return 0;
}

